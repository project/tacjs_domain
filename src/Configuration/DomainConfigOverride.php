<?php

namespace Drupal\tacjs_domain\Configuration;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Allows overriding configuration for a domain.
 *
 * @package Drupal\tacjs_domain\Configuration
 */
class DomainConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a DomainConfigOverride object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Returns config overrides.
   *
   * @param array $names
   *   A list of configuration names that are being loaded.
   *
   * @return array
   *   An array keyed by configuration name of override data. Override data
   *   contains a nested array structure of overrides.
   */
  public function loadOverrides($names = []) {
    $overrides = [];
    if (in_array('tacjs.settings', $names)) {
      // Get the domain negotiator. Note that injecting the domain negotiator
      // into the service created a circular dependency error, so we load from
      // the core service manager.
      /** @var \Drupal\domain\DomainNegotiator $negotiator */
      // @phpstan-ignore-next-line
      $negotiator = \Drupal::service('domain.negotiator');
      $domain = $negotiator->getActiveDomain();
      if (!empty($domain)) {
        $domain_key = $domain->id();
        $settings = $this->configFactory->get(
          'tacjs_domain.settings.' . $domain_key);
        // Override the default TacJS configuration.
        $overrides['tacjs.settings'] = $settings->getRawData();
      }
    }
    return $overrides;
  }

  /**
   * The string to append to the configuration static cache name.
   *
   * @return string
   *   A string to append to the configuration static cache name.
   */
  public function getCacheSuffix() {
    return 'DomainTacJsConfigOverrider';
  }

  /**
   * Gets the cacheable metadata associated with the config factory override.
   *
   * @param string $name
   *   The name of the configuration overrides to get metadata for.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   A cacheable metadata object.
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * Creates a configuration object for use during installation/synchronization.
   *
   * If the overrider stores its overrides in configuration collections, then
   * it can have its own implementation of
   * \Drupal\Core\Config\StorableConfigBase. Configuration overriders can link
   * themselves to a configuration collection by listening to the
   * \Drupal\Core\Config\ConfigEvents::COLLECTION_INFO event and adding the
   * collections they are responsible for. Doing this will allow installation
   * and synchronization to use the overrider's implementation of
   * StorableConfigBase.
   *
   * @param string $name
   *   The configuration object name.
   * @param string $collection
   *   The configuration collection.
   *
   * @return \Drupal\Core\Config\StorableConfigBase|null
   *   The configuration object for the provided name and collection.
   *
   * @see \Drupal\Core\Config\ConfigCollectionInfo
   * @see \Drupal\Core\Config\ConfigImporter::importConfig()
   * @see \Drupal\Core\Config\ConfigInstaller::createConfiguration()
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}

<?php

namespace Drupal\tacjs_domain\Form\Steps;

use Drupal\Core\Config\Config;
use Drupal\tacjs\Form\Steps\ManageDialog;

/**
 * Manage TacJS configuration for a domain.
 *
 * @package Drupal\tacjs_domain\Form\Steps
 */
class DomainManageDialog extends ManageDialog {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_domain_manage_dialog';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $domain_id = $this->getRequest()->get('domain_id');
    return ['tacjs_domain.settings.' . $domain_id];
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfig() {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_domain.settings.' . $domain_id);
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, Config $config) {
    $domain_id = $this->getRequest()->get('domain_id');

    $form['domain_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $domain_id,
    ];

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Domain: @domain_id', ['@domain_id' => $domain_id]),
      '#weight' => -100,
    ];

    parent::addFormElements($form, $config);
  }

}

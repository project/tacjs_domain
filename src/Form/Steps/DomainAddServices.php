<?php

namespace Drupal\tacjs_domain\Form\Steps;

use Drupal\Core\Config\Config;
use Drupal\tacjs\Form\Steps\AddServices;

/**
 * Manage TacJS services for a domain.
 *
 * @package Drupal\tacjs_domain\Form\Steps
 */
class DomainAddServices extends AddServices {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $domain_id = $this->getRequest()->get('domain_id');
    return [
      'tacjs_domain.settings.' . $domain_id,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_domain_settings_form';
  }

  /**
   * Get configuration.
   */
  protected function getConfig() {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_domain.settings.' . $domain_id);
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }
    return $config;
  }

  /**
   * Add elements to form.
   */
  protected function addFormElements(array &$form, Config $config) {
    $domain_id = $this->getRequest()->get('domain_id');

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Domain: @domain_id', ['@domain_id' => $domain_id]),
      '#weight' => -100,
    ];

    parent::addFormElements($form, $config);
  }

}

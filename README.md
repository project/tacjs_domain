# TacJS for Domains

This module allows configuring TarteAuCitron services on a multi-domain Drupal
website.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/tacjs_domain).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/tacjs_domain).


## Requirements

This module requires the following modules:

- [TacJS](https://www.drupal.org/project/tacjs)
- [Domain](https://www.drupal.org/project/domain)


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Go to `/admin/config/domain/tacjs_domain` to configure
   the TacJS module settings per domain.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
